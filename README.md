<h1 align="center">
  Setup 💻
</h1>

[![Status do Repositório](https://img.shields.io/badge/Repositório%20-Maintained-dark%20green.svg)](https://github.com/alexandresantosal91/setup)
[![Status do Site](https://img.shields.io/badge/Website%20Status-Online-green)](https://alexandresantosal91.github.io/setup/)
[![Autor](https://img.shields.io/badge/Author-Alexandre%20Santos-blue.svg)](https://www.linkedin.com/in/alexandresantosal/)
[![macbook-m1](https://img.shields.io/badge/MacBook-Air_M1_2021%20-blue.svg)](https://www.apple.com/br/macbook-air-m1/)
[![Lançamento Mais Recente](https://img.shields.io/badge/Latest%20Release-28%20maio%20de%202023-yellow.svg)](https://github.com/alexandresantosal91/alexandresantosal91.github.io/commits/main)
[![NPM](https://img.shields.io/npm/l/react)](https://github.com/alexandresantosal91/setup/blob/main/LICENSE)

<p align="center">
  <img alt="page" src="assets/image/page.png" width="700">
</p>

<h4 align="center"><a href="https://alexandresantosal91.github.io/setup/" target="_blank">Clique para visitar o projeto</a></h4>

 <p align="justify">Entre em contato comigo pelo e-mail alexandresantos_al@hotmail.com se tiver algum comentário ou ideia para o site. Se gostou, deixe uma avaliação de estrelas!</p>

